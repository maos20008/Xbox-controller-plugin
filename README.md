# Xbox-controller-plugin

You can use this script header file in your C++ DirectX 11 games if you prefer to use a Xbox controller to play your games. Read comments in the header file on how to setup with your C++ Game project and enjoy!

Requirements:

[DirectX Software Development Kit]( https://www.microsoft.com/en-us/download/details.aspx?id=6812)

[Visual Studio 2015 or later](https://www.visualstudio.com/vs/community/)

[Xbox One Controller](https://www.amazon.com/Microsoft-Xbox-One-Wireless-Controller-Black/dp/B00Q3FLOP6/ref=sr_1_7?)

![xbox controller](https://user-images.githubusercontent.com/18353476/39464573-17d1bf34-4cd3-11e8-8ee0-634f07337506.png)
